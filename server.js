const express = require("express");
const fs = require("fs");
const base62 = require("base62/lib/ascii");
const { raw } = require("express");
const { count } = require("console");

const app = express();
const port = 5000;


var counter;

const saveData = (url, expiresOn, shortLink) => {
    const rawData = fs.readFileSync("./json/shortLinks.json");
    let dataToSave = {
        url, expiresOn, shortLink
    }
    console.log(rawData.byteLength);
    if (rawData.byteLength > 0) {
        dataToSave = [...JSON.parse(rawData), dataToSave];
    } else {
        dataToSave = [dataToSave];
    }
    fs.writeFileSync("./json/shortLinks.json", JSON.stringify(dataToSave));
    console.log("Data Save Successful");
    return "SUCCESS";
}

const generateShortLink = (url, expiresOn) => {
    const rawData = fs.readFileSync("./json/shortLinks.json");
    let shortLink;
    if (rawData.byteLength > 0) {
        console.log(JSON.parse(rawData));
        counter = 1000000 + JSON.parse(rawData).length;
        var existingData = JSON.parse(rawData).filter(
            (data) => { return (data.url == url && data.expiresOn == expiresOn) }
        );
        console.log("Existing Data", existingData);
        if (existingData.length > 0) {
            shortLink = existingData[0].shortLink;
        } else {
            shortLink = base62.encode(counter).substr(0, 7);
            saveData(url, expiresOn, shortLink)
            counter++;
        }
    } else {
        counter=1000000
        shortLink = base62.encode(counter).substr(0, 7);
        saveData(url, expiresOn, shortLink)
        counter++;
    }
    console.log("counter", counter);
    return shortLink;
}

app.get("/", (req, res) => {
    res.send("Hello World");
})

app.get("/s/:param", (req, res) => {
    res.send(req.params);
});

app.get("/shortLink", (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    //console.log(req);
    console.log(req.query);
    res.status(200);
    let shortLink = generateShortLink(req.query.url, req.query.expiresOn);
    let response = {
        action: "addLink",
        response: "SUCCESS",
        shortLink: "/s/" + shortLink
    }
    res.send(response)
})

app.get("/getLink", (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    var date = new Date();
    var today = date.toISOString().split("T")[0];
    console.log(today);
    var shortLink = req.query.shortLink.replace("/s/","");
    var isfound = false;
    var url="";
    const rawData = fs.readFileSync("./json/shortLinks.json");
    if (rawData.byteLength > 0) {
        console.log(JSON.parse(rawData), shortLink);
        var existingData = JSON.parse(rawData).filter(
            (data) => { return (data.shortLink == shortLink) }
        );
        console.log("Existing Data", existingData);
        
        if (existingData.length > 0) {
            url = existingData[0].url;
            isfound=true;
        } else {
            isfound=false;
        }
    } else {
        isfound=false;
    }

    if(isfound) {
        res.status(200).send({
            message: "SUCCESS",
            url,
            shortLink
        });
    } else {
        res.status(404).send({
            message: "redirect not found",
            shortLink
        })
    }

})

app.listen(process.env.PORT || 5000, () => {
    console.log("Server listening to port: " + port);
});